FROM ruby:slim-buster

EXPOSE 4567/TCP

RUN apt update && apt upgrade -y
RUN mkdir /var/www
RUN chown -R www-data /var/www
USER www-data
WORKDIR /var/www
COPY Gemfile .
COPY config.ru .
COPY helloworld.rb .
RUN bundle install

ENTRYPOINT ["ruby"]
CMD ["helloworld.rb"]
